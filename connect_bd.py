from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


from test import Base


class ConnectBd:
    engine = create_engine('postgresql://postgres:234234Qq@mydatabase.cb6bbbopnp73.us-east-1.rds.amazonaws.com:5432/mydb')

    def __init__(self):
        self.create_table()
        session = sessionmaker(bind=self.engine)
        self.s = session()

    def create_table(self):
        Base.metadata.create_all(self.engine)


bd = ConnectBd()
