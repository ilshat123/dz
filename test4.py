import psycopg2


class Edge:
    def __init__(self):
        self.first_site_name = None
        self.second_site_name = None


class Page:
    def __init__(self):
        self.page_name = None
        self.page_rang = None
        self.input_pages = []

    def add_input_page(self, page):
        self.input_pages.append(page)

    def __hash__(self):
        return hash(self.page_name)

    def __eq__(self, other):
        return self.page_name == other.page_name


def get_pages():

    pages = []
    for i in range(1, 3):
        page = Page()
        page.page_name = str(i)
        pages.append(page)
    return pages


def get_edges():
    edges = []
    edge1 = Edge()
    edge1.first_site_name = "1"
    edge1.second_site_name = "2"
    edges.append(edge1)
    edge1 = Edge()
    edge1.first_site_name = "2"
    edge1.second_site_name = "1"
    edges.append(edge1)
    return edges


def get_edges2(cursor):
    cursor.execute("select first_site_name, second_site_name from edge")
    rows = cursor.fetchall()
    edges = []
    for elem in rows:
        edge = Edge()
        edge.first_site_name = elem[0]
        edge.second_site_name = elem[1]
        edges.append(edge)
    return edges
    # print(rows)


def get_pages2(edges):
    page_names = set()
    for i in edges:
        page_names.add(i.first_site_name)
        page_names.add(i.second_site_name)

    pages = []
    for name in page_names:
        page = Page()
        page.page_name = name
        pages.append(page)
    return pages


def get_new_page_rang(page, d):
    num1 = 0
    for input_page in page.input_pages:
        n1 = input_page.page_rang / len(page.input_pages)
        num1 += n1
    num = (1 - d) + d * num1
    return num


def check_epsilon(pages):
    e = 0.1
    all_sum = sum([page.page_rang for page in pages])
    return (abs(1 - all_sum / len(pages))) > e


def test123(pages, edges, d, start_amount, iter_num):
    for page in pages:
        page.page_rang = start_amount
    pages_dict = {elem.page_name: elem for elem in pages}

    for elem in edges:
        first_page = pages_dict.get(elem.first_site_name)
        second_page = pages_dict.get(elem.second_site_name)
        second_page.add_input_page(first_page)

    # for i in range(iter_num):
    while check_epsilon(pages):
        for page in pages:
            page.page_rang = get_new_page_rang(page, d)

    return pages


if __name__ == '__main__':
    con = psycopg2.connect(
        database='mydb',
        user='postgres',
        password='234234Qq',
        host='mydatabase.cb6bbbopnp73.us-east-1.rds.amazonaws.com',
        port=5432,
    )
    cursor = con.cursor()
    get_edges2(cursor)



    edges = get_edges2(cursor)
    pages = get_pages2(edges)

    # pages = get_pages()
    # print(pages)
    # edges = get_edges()
    d = 0.85
    start_amount = 0
    iter_num = 20
    pages = test123(pages, edges, d, start_amount, iter_num)

    for i in pages:
        print(i.page_name, i.page_rang)



